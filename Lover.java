/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package girlfirend;

/**
 *
 * @author Arpenteur
 */
public class Lover extends Person {
    private Person lover;
    
    public Lover(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public Lover(Person lover, String firstName, String lastName) {
        super(firstName, lastName);
        this.lover = lover;
    }
    
    public void setNewLover(Person p)
    {
        System.out.println("[INFO] " + super.toString() + " is now with " + p.toString());
        lover = p;
    }
    
    @Override 
    public String toString() {
        return super.toString();
    }
    
    public void IsWith()
    {
        if(lover != null)
        {
            System.out.println(super.toString() + " is with " + lover.toString());
        }
        else
        {
            System.out.println(super.toString() + " is with no one!");
        }
    }
}
