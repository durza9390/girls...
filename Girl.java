/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package girlfirend;

/**
 *
 * @author Arpenteur
 */

public class Girl extends Lover {
    
    Mood mood = Mood.normal;
    
    public Girl(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public void setMood(Mood mood) {
        System.out.println(this.toString() + " " + "is now " + mood.toString());
        this.mood = mood;
    }
    
    @Override
    public void setNewLover(Person p)
    {
        if(mood == Mood.normal || mood == Mood.happy)
        {
            super.setNewLover(p);
        }
        else
        {
            System.out.println("[ERROR] She's not in the mood to accept to be with " + p.toString());
        }
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
